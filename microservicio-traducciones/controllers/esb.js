var esbService = require('../services/esb')

function postComplemento(req, res){
    esbService.postComplemento(req.token, req.body.nombre, req.body.correo, req.body.complemento, (msg, err)=>{
        if(err) return res.status(500).send({estado: '500', mensaje: 'Error al procesar la solicitud de POST Complemento'})
        return res.status(200).send(msg)
    })
}

function getComplementos(req, res){
    esbService.getComplementos(req.token, (msg, err)=>{
        if(err)return res.status(500).send({estado: '500', mensaje : 'Error al procesar la solicitud de GET Complementos'})
        return res.status(200).send(msg)
    })
}

function getComplemento(req, res) {
    esbService.getComplemento(req.token, req.params.complementoId, (msg, err) => {
        if(err)return res.status(500).send({estado: '500', mensaje : "Error al procesar la solicitud de Get Complemento"})
        return res.status(200).send(msg)
    })
}

function postAprobarTraduccionCadena(req, res){
    esbService.postAprobarTraduccionCadena(req.token, req.body.nombreComplemento, req.body.idComplemento, req.body.nombreLocalizacionOriginal, req.body.nombreLocalizacionTraducida,
        req.body.cadenaOriginal, req.body.cadenaTraducida, req.body.nombre, req.body.correo, (msg, err)=>{
        if(err) return res.status(500).send({estado: '500', mensaje: 'Error al procesar la solicitud de POST Aprobar Traduccion Cadena'})
        return res.status(200).send(msg)
    })
}

function getCatalogoComplemento(req, res) {
    esbService.getCatalogoComplemento(req.token, req.params.complementoId, (msg, err) => {
        if(err)return res.status(500).send({estado: '500', mensaje : "Error al procesar la solicitud de Get Catalogo Complemento"})
        return res.status(200).send(msg)
    })
}

function postAgregarTraduccionCadena(req, res){
    esbService.postAgregarTraduccionCadena(req.token, req.body.nombreComplemento, req.body.idComplemento, req.body.nombreLocalizacionOriginal, req.body.nombreLocalizacionTraducida,
        req.body.cadenaOriginal, req.body.cadenaTraducida, req.body.nombre, req.body.correo, (msg, err)=>{
        if(err) return res.status(500).send({estado: '500', mensaje: 'Error al procesar la solicitud de POST Agregar Traduccion Cadena'})
        return res.status(200).send(msg)
    })
}

function deleteComplemento(req, res) {
    esbService.deleteComplemento(req.token, req.params.complementoId, (msg, err) => {
        if(err)return res.status(500).send({estado: '500', mensaje : "Error al procesar la solicitud de Delete Complemento"})
        return res.status(200).send(msg)
    })
}

module.exports = {
    getComplementos,
    getComplemento,
    postComplemento,
    getCatalogoComplemento,
    postAprobarTraduccionCadena,
    postAgregarTraduccionCadena,
    deleteComplemento
}