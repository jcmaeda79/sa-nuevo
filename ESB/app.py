#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import jwt
import time
import json
import os
from datetime import datetime, date
from flask import Flask, render_template, request, Response, jsonify
from requests.exceptions import ConnectionError


app = Flask(__name__)

class ESB():
    def __init__(self, Nombre):
        self.Nombre = Nombre


"""
*   DEFINICIÓN DE PUERTOS PARA CADA SERVICIO
*   5001 Micro servicio de almacenamiento
*   5002 Micro servicio de traducción
*   5003 Micro servicio de archivos_traducidos
*   5004 Servidor JWT
*   5005 ESB
"""



"""
*   Ruta con la que se le van a hacer peticiones al ESB
*
*   Se espera recibir un JSON con los siguientes 4 elementos:
* 
*   Token: token codificado por medio de la llave privada del servidor JWT (String)
*   Ruta: ruta del microservicio a consumir (String)
*   Tipo: tipo de comunicación que se desea realizar por medio de la ruta (POST,GET, etc) (String)
*   Parámetros: parámetros que necesita enviar para consumir la ruta en el microservicio (JSON)
*"""

"""
* ********************Pruebas para generar en formato Unix el tiempo de expiración en el payload del token *************************
    ***se puede generar en el formato unix
    print(time.time() + 100) 
    ***se puede obtener la fecha y hora exacta de ese formato unix
    print(datetime.fromtimestamp(int("1569715162")).strftime('%Y-%m-%d %H:%M:%S'))
************************************************************************************************************
*"""
@app.route('/post/comunicacion', methods=['POST'])
def SolicitudESB():

    solicitud = request.get_json(force=True, silent = True) #Le digo que si hay error al parsear el JSON no muera, sino que retorne None
    #solicitud = request.get_json( silent = True) #Le digo que si hay error al parsear el JSON no muera, sino que retorne None
    #Verifico si se puede parsear el JSON recibido
    if solicitud == None:
        return jsonify(
                    estado='500',
                    mensaje='Existe un error en la estructura del JSON con el que se hace la solicitud al ESB',
                   )

    #Si sí se pudo parsear la solicitud a JSON obtengo los datos que se envían
    token = solicitud.get('token')#solicitud.get('token') se verifica primero el token
    funcionSolicitada = solicitud.get('funcionSolicitada')

    validarToken = getValidationToken(token, funcionSolicitada)
    if validarToken != True: #si no retorna True hubo un problema con el token
        return validarToken     #retorna un JSON con el error
    
    url = solicitud.get('url')
    tipo = solicitud.get('tipo')
    parametros = solicitud.get('parametros')

    try:

        if tipo == 'POST' or tipo == 'post':  
            response = requests.post(url = url, params=parametros)
            return response.json()

        elif tipo == 'GET' or tipo == 'get':
            response = requests.get(url = url, params=parametros)
            return response.json()

        elif tipo == 'DELETE' or tipo == 'delete':
            response = requests.delete(url = url, params=parametros)
            return response.json()


    except ConnectionError as e:
        return jsonify(
            estado='404',
            mensaje='No se pudo establecer la comunicacion: ' + url,
        )
    except:
        return jsonify(
            estado='404',
            mensaje='Excepcion inesperada en el ESB',
        )
    #Si llega hasta aquí es porque el tipo de solicitud NO es correcto (POST, GET, DELETE)
    return jsonify(
                    estado='404',
                    mensaje='El tipo de solicitud es incorrecta',
                   )


#Función que retorna True si es valido el token sino retorna el error
#el parametro token es la llave privada que envia el microservicio por medio de su payload
def getValidationToken(token, funcionSolicitada):
    if token == None: #que el token no venga null
        return jsonify(
                    estado='500',
                    mensaje='Existe un error en el token enviado viene nulo',
                   )
    f =open("/home/publickeys/publickey.ssh","r")
    content = f.read()
    f.close()          
    try:
        decoded = jwt.decode(token, content, algorithms='RS256')
        auth =  decoded["auth"]
        if auth == "true":
            scopes = decoded["scopes"]
            #recorrer scopes
            for x in scopes:
            #validar que la funcion Solicitada existe el scope
                if x == funcionSolicitada:
                    #retornar error si no existe
                    return True
            return jsonify(
                estado='500',
                mensaje='El scope es inválido.',
            )        
        return jsonify(
            estado='500',
            mensaje='El token es inválido.',
        )
    except jwt.ExpiredSignatureError:
        #el tiempo ha expirado será comparado con el tiempo actual UTC , timegm(datetime.utcnow().utctimetuple()))
        return jsonify(
                    estado='500',
                    mensaje='El token ha expirado.',
                   )
    except jwt.DecodeError:
        #tira una excepción cuando la validación del token falla
        return jsonify(
                    estado='500',
                    mensaje='La validación del token ha fallado no viene porque el token viene alterado.',
                   )

    return False


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
    #print(time.time() + 60) 
    #print(datetime.fromtimestamp(int("1572393664")).strftime('%Y-%m-%d %H:%M:%S'))