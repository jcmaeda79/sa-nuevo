CREATE DATABASE  IF NOT EXISTS `Almacenamiento` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `Almacenamiento`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: Almacenamiento
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `BitacoraCambio`
--

DROP TABLE IF EXISTS `BitacoraCambio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BitacoraCambio` (
  `idBitacoraCambio` int(11) NOT NULL AUTO_INCREMENT,
  `idDetalleComplemento` int(11) NOT NULL,
  `IdEstado` int(11) DEFAULT NULL,
  `FechaCambio` datetime DEFAULT NULL,
  `nombreusr` varchar(100) DEFAULT NULL,
  `correousr` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`idBitacoraCambio`,`idDetalleComplemento`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Bitacora de cambios en la cadena, usuarios ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BitacoraCambio`
--

LOCK TABLES `BitacoraCambio` WRITE;
/*!40000 ALTER TABLE `BitacoraCambio` DISABLE KEYS */;
INSERT INTO `BitacoraCambio` VALUES (1,1,1,'2019-10-21 18:19:04','jcmaeda','jcmaeda@gmail.com'),(2,2,1,'2019-10-21 18:21:41','jcmaeda','jcmaeda@gmail.com'),(3,3,1,'2019-10-21 18:24:49','jcmaeda','jcmaeda@gmail.com'),(4,4,1,'2019-10-22 13:35:38','jcmaeda','jcmaeda@gmail.com'),(5,5,1,'2019-10-22 13:36:38','jcmaeda','jcmaeda@gmail.com'),(6,6,1,'2019-10-22 13:37:26','jcmaeda','jcmaeda@gmail.com'),(7,7,1,'2019-10-22 19:16:02','jcmaeda','jcmaeda@gmail.com'),(8,10,5,'2019-10-26 01:44:24','Juan Maeda','jcarlosmaeda@gmail.com'),(9,11,5,'2019-10-26 01:44:32','Juan Maeda','jcarlosmaeda@gmail.com'),(10,11,6,'2019-10-26 03:18:28','Fernanda Santos','fsantos@gmail.com'),(11,12,1,'2019-10-26 03:34:05','Juan Carlos Maeda','jcarlosmaeda@gmail.com'),(12,13,5,'2019-10-26 03:42:57','Juan Carlos Maeda','jcarlosmaeda@gmail.com'),(13,14,1,'2019-10-26 13:41:49','Juan Carlos Maeda','jcarlosmaeda@gmail.com'),(14,15,5,'2019-10-26 13:42:08','Juan Carlos Maeda','jcarlosmaeda@gmail.com'),(15,15,6,'2019-10-26 13:47:42','Juan Carlos Maeda','jcarlosmaeda@gmail.com'),(16,15,6,'2019-10-26 13:48:30','Juan Carlos Maeda','jcarlosmaeda@gmail.com'),(17,10,6,'2019-10-26 13:48:52','Juan Carlos Maeda','jcarlosmaeda@gmail.com');
/*!40000 ALTER TABLE `BitacoraCambio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cadena`
--

DROP TABLE IF EXISTS `Cadena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cadena` (
  `idDetalleComplemento` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Correlativo del detalle del complemento de la cadena',
  `idComplemento` int(11) NOT NULL COMMENT 'Identificador del complemento',
  `idLocalizacion` int(11) NOT NULL COMMENT 'Idioma o lenguaje de la cadena',
  `idEstado` int(11) NOT NULL COMMENT 'Estado de la cadena',
  `cadena` longtext COMMENT 'Cadena',
  `nombreusr` varchar(100) DEFAULT NULL COMMENT 'Nombre de usuario que trabajo la cadena',
  `correousr` varchar(150) DEFAULT NULL,
  `idCadenaOriginal` int(11) DEFAULT NULL COMMENT 'Cadena original a la que pertence la traduccion, para el primer caso o la cadena orignal este valor es null',
  PRIMARY KEY (`idDetalleComplemento`,`idComplemento`,`idLocalizacion`,`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Guarda las cadenas de los complementos, por cada complemento N cadenas ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cadena`
--

LOCK TABLES `Cadena` WRITE;
/*!40000 ALTER TABLE `Cadena` DISABLE KEYS */;
INSERT INTO `Cadena` VALUES (1,8,5,1,'Login','jcmaeda','jcmaeda@gmail.com',NULL),(2,8,5,1,'User','jcmaeda','jcmaeda@gmail.com',NULL),(3,8,5,1,'Password','jcmaeda','jcmaeda@gmail.com',NULL),(4,8,5,1,'Return','jcmaeda','jcmaeda@gmail.com',NULL),(5,8,5,1,'Welcome','jcmaeda','jcmaeda@gmail.com',NULL),(6,6,5,1,'Welcome','jcmaeda','jcmaeda@gmail.com',NULL),(7,6,5,1,'Menu','jcmaeda','jcmaeda@gmail.com',NULL),(10,6,3,5,'Menu','Juan Maeda','jcarlosmaeda@gmail.com',7),(11,6,3,5,'Bienvenido','Juan Maeda','jcarlosmaeda@gmail.com',6),(12,6,5,1,'About','Juan Carlos Maeda','jcarlosmaeda@gmail.com',NULL),(13,6,3,5,'Acerca de','Juan Carlos Maeda','jcarlosmaeda@gmail.com',12),(14,6,5,1,'Close','Juan Carlos Maeda','jcarlosmaeda@gmail.com',NULL),(15,6,3,5,'Cerrar','Juan Carlos Maeda','jcarlosmaeda@gmail.com',14);
/*!40000 ALTER TABLE `Cadena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Complemento`
--

DROP TABLE IF EXISTS `Complemento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Complemento` (
  `idComplemento` int(11) NOT NULL AUTO_INCREMENT,
  `Complemento` varchar(60) NOT NULL,
  `nombreusr` varchar(100) NOT NULL COMMENT 'Nombre dle usuario que ha creado el complemento',
  `correousr` varchar(150) NOT NULL COMMENT 'Correo del usuario que ha creado el contenedor',
  `idEstado` int(11) DEFAULT NULL COMMENT 'Llave foranea de Estados',
  PRIMARY KEY (`idComplemento`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla en donde se guardan los complementos, y la o las localizaciones originales, aunque debe ser una sola ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Complemento`
--

LOCK TABLES `Complemento` WRITE;
/*!40000 ALTER TABLE `Complemento` DISABLE KEYS */;
INSERT INTO `Complemento` VALUES (6,'WordPress Menu','Juan Carlos Maeda','jcarlosmaeda@gmail.com',NULL),(8,'WordPress Login','jcmaeda','jcmaeda@gmail.com',1),(9,'WordPress Galery','Juan Carlos Maeda','jcarlosmaeda@gmail.com',1);
/*!40000 ALTER TABLE `Complemento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ComplementoLocalizacion`
--

DROP TABLE IF EXISTS `ComplementoLocalizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ComplementoLocalizacion` (
  `idComplemento` int(11) NOT NULL,
  `idLocalizacion` int(11) NOT NULL,
  `idEstado` int(11) NOT NULL,
  `Ruta` varchar(500) DEFAULT NULL COMMENT 'Ruta del archivo que guardara el complemento',
  PRIMARY KEY (`idComplemento`,`idLocalizacion`,`idEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Por cada complemento se debe guardar la localizacion y el lenguaje, la ruta del archivo';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ComplementoLocalizacion`
--

LOCK TABLES `ComplementoLocalizacion` WRITE;
/*!40000 ALTER TABLE `ComplementoLocalizacion` DISABLE KEYS */;
INSERT INTO `ComplementoLocalizacion` VALUES (6,3,1,NULL),(6,5,1,NULL),(8,3,1,NULL),(8,5,1,NULL),(9,5,1,NULL);
/*!40000 ALTER TABLE `ComplementoLocalizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Localizacion`
--

DROP TABLE IF EXISTS `Localizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Localizacion` (
  `idLocalizacion` int(11) NOT NULL AUTO_INCREMENT,
  `Localizacion` varchar(45) DEFAULT NULL,
  `Internalizacion` varchar(45) DEFAULT NULL,
  `idEstado` int(11) DEFAULT NULL COMMENT 'LLave foranea de los estados',
  PRIMARY KEY (`idLocalizacion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla que guarda las localizaciones que se pueden utilizar en el sistema	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Localizacion`
--

LOCK TABLES `Localizacion` WRITE;
/*!40000 ALTER TABLE `Localizacion` DISABLE KEYS */;
INSERT INTO `Localizacion` VALUES (3,'UA-08','US-EN',1),(5,'UA-09','US-ES',1);
/*!40000 ALTER TABLE `Localizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL AUTO_INCREMENT,
  `Estado` varchar(45) NOT NULL,
  PRIMARY KEY (`idEstado`),
  UNIQUE KEY `idEstado_UNIQUE` (`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Tabla de Estados de los Complementos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(3,'Inactivo'),(5,'Traducido'),(6,'Aprobado');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'Almacenamiento'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoEliminaComplemento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoEliminaComplemento`(IN iidcomplemento int,IN iComplemento varchar(60),IN ilocalizacion varchar(100))
BEGIN
	Declare	varidcomplemento int;
    Declare varidlocalizacion int;
	if iidcomplemento is null then
		select idComplemento into varidcomplemento from Almacenamiento.Complemento where Complemento = iComplemento;
	else 
		set varidcomplemento = iidcomplemento;
    end if;
    if ilocalizacion is null then
		delete from Almacenamiento.ComplementoLocalizacion where idComplemento = varidcomplemento;   
    else
		select idLocalizacion INTO varidlocalizacion from Almacenamiento.Localizacion where Localizacion = ilocalizacion;
        delete from Almacenamiento.ComplementoLocalizacion where idComplemento = varidcomplemento and idLocalizacion = varidlocalizacion; 
    end if;   
    if (select count(*) from Almacenamiento.ComplementoLocalizacion where idComplemento = varidcomplemento) = 0 then
      delete from Almacenamiento.Complemento where idComplemento = varidcomplemento;
	  select '200' Estado, 'OK' Mensaje, 'data' data, 'Se elimino el complemento y la localizacion correctamente.' Nombre;
   else
      select '401' Estado,'Error al eliminar el complemento' Mensaje;
	end if; 
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoEliminarCatalogo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoEliminarCatalogo`(IN idCatalogo int, IN localizacion varchar(25), IN Internacionalizacion varchar(25))
BEGIN
	declare varidCatalogo int;
    select idLocalizacion into varidCatalogo from Almacenamiento.Localizacion where (Localizacion = localizacion or Internalizacion = Internacionalizacion);
	delete from Almacenamiento.Localizacion where idLocalizacion = varidCatalogo;
    if (select count(*) from Almacenamiento.Localizacion where (idLocalizacion = idCatalogo or Localizacion = localizacion or Internalizacion = Internacionalizacion)) = 0 then
    	select '200' Estado, 'OK' Mensaje, 'data' data, 'Se elimino correctamente la Localizacion' Nombre;
	else
			select '401' Estado,'Error al eliminar el catalogo' Mensaje;
	end if; 
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoInsertaCadena` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_almacenamientoInsertaCadena`(
	_Complemento varchar(60),
    _Localizacion varchar(10),
  	_cadena varchar(16383),
    _nombreusr varchar(100),
    _correousr varchar(150)
)
BEGIN

	declare _iddetalleComplemento int;
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
		select 401 estado,'Error al insertar la cadena ' mensaje;               
	  END;
      If (select count(*) from Localizacion where (Localizacion = _Localizacion) ) = 0 Then
			call sp_almacenamientoInsertaLocalizacion(_Localizacion,_internacionalizacion);
        end if;
      
		set  @idComplemento = (select idComplemento  from Complemento where Complemento = _Complemento limit 1);

        
		set @idLocalizacion  = (select  idlocalizacion from Localizacion where (Localizacion = _Localizacion) limit 1); 
        set @idEstado = (select  idEstado from estado where Estado = 'Activo' limit 1);
        
        if (select count(*) from Cadena where IdComplemento = @idComplemento and idlocalizacion = @idlocalizacion and cadena = _cadena) = 0 then
			insert into Cadena (IdComplemento, idLocalizacion, IdEstado, cadena,nombreusr, correousr)
			values (@idComplemento,@idLocalizacion,@idEstado,_cadena,_nombreusr,_correousr);      
		end if;
		
		set _iddetalleComplemento = (select idDetalleComplemento  from Cadena where IdComplemento = @idComplemento and idlocalizacion = @idlocalizacion and cadena = _cadena);
        
      
        insert into BitacoraCambio (idDetalleComplemento,IdEstado,FechaCambio,nombreusr,correousr)
		values (_iddetalleComplemento,@idEstado,now(),_nombreusr,_correousr);    
 		
        
        select 200 estado, concat('Cadena agregada correctamente al complemento ',_Complemento) Mensaje;
      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoInsertaComplemento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoInsertaComplemento`(IN nombreusr varchar(100),IN correousr varchar(150),IN icomplemento varchar(50),IN ilocalizacion varchar(15))
BEGIN
	declare vaidEstado int;
    declare vaidLocalizacion int;
    declare vaidComplemento int;
    declare Ruta varchar(500);
         
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
			select 401 estado, 'Error al crear complemento' mensaje;				 	          
	  END;
     
     select IdEstado INTO vaidEstado from Almacenamiento.estado where Estado = 'Activo';
    if (select count(*) from Almacenamiento.Complemento where Complemento = iComplemento) = 0 then
		insert into Almacenamiento.Complemento (Complemento,nombreusr,correousr,IdEstado) values (icomplemento,nombreusr,correousr,vaidEstado);   
	end if;
    set Ruta = null;
    select idComplemento INTO vaidComplemento from Almacenamiento.Complemento where Complemento = iComplemento;
    select idlocalizacion INTO vaidLocalizacion from Almacenamiento.Localizacion where localizacion = ilocalizacion;
    insert into Almacenamiento.ComplementoLocalizacion (idComplemento,idLocalizacion,idEstado,Ruta) 
    values(vaidComplemento,vaidLocalizacion,vaidEstado,Ruta);
        
    if (select count(*) from Almacenamiento.Complemento where Complemento = Complemento) > 0 then
    	select '200' Estado, 'OK' Mensaje, concat('Se elimino correctamente el Complemento ', iComplemento) Nombre;
	end if;    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoInsertaEstado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoInsertaEstado`(IN inEstado varchar(25))
BEGIN 
	if (select count(*) from Almacenamiento.estado where Estado = inEstado) = 0 then
		INSERT INTO Almacenamiento.estado
		(     
		Estado)
		VALUES
		(
		inEstado);
    end if;
	if exists (select * from Almacenamiento.estado where Estado = inEstado) then
		select '200' Estado, 'OK' Mensaje, 'data' data, inEstado Nombre;
    else
		select '401' Estado,'Error al crear el estado' Mensaje;
    end if;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoInsertaLocalizacion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoInsertaLocalizacion`(in localizacion varchar(10), in internacionalizacion varchar(10) )
BEGIN
	declare varEstado int;
    select idEstado INTO varEstado from estado where Estado = 'Activo';
   	if (select count(*) from Localizacion where (Localizacion  = localizacion and Internalizacion = internacionalizacion)) = 0 then
		Insert Into Localizacion
			(Localizacion,Internalizacion,idEstado)
			Values
			(localizacion,internacionalizacion,varEstado);
		end if;
        if exists (select idLocalizacion from Localizacion where (Localizacion = localizacion or Internalizacion = internacionalizacion)) then
			select '200' Estado, 'OK' Mensaje, 'data' data, localizacion Nombre;
		else
			select '401' Estado,'Error al crear el la localizacion' Mensaje;
		end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoInsertaTraduccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoInsertaTraduccion`(
_Complemento varchar(60),_LocalizacionOriginal varchar(10),_Localizacion varchar(10),
	_cadenaOrginal varchar(16383),_cadena varchar(16383),_nombreusr varchar(100),_correousr varchar(150),
    _opcion int
)
BEGIN
	  declare _idComplemento int;
      declare _idLocalizacion int;
      declare _idCadenaOriginal int;
      declare _idestado int;
      declare _iddetallecomplemento int;
      declare _idLocalizacionOriginal int;
      DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
		select 401 estado,'Error al insertar la traduccion ' mensaje;               
	  END;
      set _idComplemento = (select idComplemento from Complemento where Complemento = _Complemento);
		set _idLocalizacion = (select idLocalizacion from Localizacion where Localizacion = _Localizacion);
		set _idLocalizacionOriginal = (select idLocalizacion from Localizacion where Localizacion = _LocalizacionOriginal);
		set _idestado = (select idestado from estado where Estado = 'Traducido');
		set _idCadenaOriginal = (select idDetalleComplemento from Cadena 
								  where idComplemento = _idComplemento 
								  and idLocalizacion = _idLocalizacionOriginal
								  and cadena = _cadenaOrginal and idCadenaOriginal is null);
      
      /*Se puede utilizar el mismo procedimiento para insertar y para aprobar una traduccion,
        el parametro opcion 1 para insertar una traducicon, y 0 para aprobarlas*/
		/*Hace las busquedas de los identificadores*/
        if _opcion = 1 then
        			
			insert into Cadena (idComplemento,idLocalizacion,idEstado,cadena,nombreusr,correousr,idCadenaOriginal)
			values (_idComplemento,_idLocalizacion,_idestado,_cadena,_nombreusr,_correousr,_idCadenaOriginal);
			
			set _iddetallecomplemento = (select iddetallecomplemento from Cadena 
											where idComplemento = _idComplemento
											and idEstado = _idestado
											and cadena = _cadena
											and idLocalizacion = _idLocalizacion);
										   
			/*insert into BitacoraCambio (idDetalleComplemento,IdEstado,FechaCambio,nombreusr,correousr)
			values (_iddetallecomplemento,_idestado,now(),_nombreusr,_correousr);
			*/
			select 200 estado, 'Traduccion insertada correctamente';
		else
			set _iddetallecomplemento = (select iddetallecomplemento 
										 from Cadena 
										 where idComplemento = _idComplemento
											and idEstado = _idestado
											and cadena = _cadena
											and idLocalizacion = _idLocalizacion);
			set _idestado = (select idestado from estado where Estado = 'Aprobado');
            select 200 estado, concat('Aprobacion de traduccion ',_cadena) mensaje;
		end if;
        insert into BitacoraCambio (idDetalleComplemento,IdEstado,FechaCambio,nombreusr,correousr)
		values (_iddetallecomplemento,_idestado,now(),_nombreusr,_correousr);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoObtenerCatalogo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoObtenerCatalogo`()
BEGIN
	SELECT 200 Estado, 'OK' Mensaje, CO.idComplemento,CO.Complemento,L.Localizacion LocalizacionOriginal,L1.Localizacion LocalizacionTraduccion, C.Cadena msgid, C1.Cadena msgstr,ifnull(count(BC.idDetalleComplemento),0) numeroAprobaciones
	from Cadena C
	inner join Cadena C1 on C.idDetalleComplemento = C1.idCadenaOriginal
	inner join Localizacion L on C.idLocalizacion = L.idLocalizacion
	inner join Localizacion L1 on C1.idLocalizacion = L1.idLocalizacion
	inner join Complemento CO on CO.idComplemento = C.idComplemento
	left join BitacoraCambio BC on BC.idDetalleComplemento = C1.idDetalleComplemento and BC.IdEstado in (select idEstado from estado where estado = 'Aprobado')
	group by CO.idComplemento,CO.Complemento,L.Localizacion,L1.Localizacion, C.Cadena , C1.Cadena;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoObtenerComplemento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoObtenerComplemento`(IN iidcomplemento int,IN icomplemento varchar(60))
BEGIN
   DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
			select 401 estado, 'Error al Obtener complemento' mensaje;				 	          
	  END;
     
      if iidcomplemento > 0 or icomplemento is not null then		
      	 SELECT 200 estado, 'OK' mensaje,idComplemento,Complemento nombre, GROUP_CONCAT(Localizacion) localizaciones
		 FROM Complemento
			INNER JOIN ComplementoLocalizacion USING (idComplemento)
			INNER JOIN Localizacion USING (idLocalizacion)
		 where idComplemento = iidcomplemento or Complemento = icomplemento
		 GROUP BY idComplemento;
	else
		SELECT 200 estado, 'OK' mensaje,idComplemento,Complemento nombre, GROUP_CONCAT(Localizacion) localizaciones
		 FROM Complemento
			INNER JOIN ComplementoLocalizacion USING (idComplemento)
			INNER JOIN Localizacion USING (idLocalizacion)
		 GROUP BY idComplemento;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_almacenamientoRetornaCadena` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_almacenamientoRetornaCadena`(
)
BEGIN
	 DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
		select 401 estado, 'Error al Retornar cadenas' mensaje;		                   
	  END;
	  	 
      CREATE TEMPORARY TABLE tmpComplemento1 
		(
			tmpid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY
		)
		SELECT 200 estado, 'OK' mensaje,idComplemento,Complemento nombreComplemento, GROUP_CONCAT(Localizacion) localizaciones,nombreusr nombre,correousr correo
		FROM Complemento
		INNER JOIN ComplementoLocalizacion USING (idComplemento)
		INNER JOIN Localizacion USING (idLocalizacion)
		GROUP BY idComplemento;

		CREATE TEMPORARY TABLE tmpComplemento 
		(
			tmpid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY
		)
		select * from tmpComplemento1;


		select estado,mensaje,idComplemento,nombreComplemento,localizaciones localizacionOriginal,nombre,correo,null cadena ,null localizacion 
		from tmpComplemento1
		where not exists (select * from Cadena where Cadena.IdComplemento = tmpComplemento1.IdComplemento)
		union all
		select distinct estado,mensaje,tmpComplemento.idComplemento,nombreComplemento,localizaciones localizacionOriginal,nombre,correo,group_concat(distinct cadena) cadena ,group_concat(distinct localizacion) localizacion
		from tmpComplemento
		inner JOIN Cadena using (idComplemento)
		inner join Localizacion using (idLocalizacion)
		group by estado,mensaje,idComplemento,nombreComplemento,localizaciones,idlocalizacion,nombre,correo;

		drop table tmpComplemento;		
		drop table tmpComplemento1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-26  9:56:34
