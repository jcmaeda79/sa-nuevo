CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoObtenerCatalogoTraducciones`()
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
		select 401 estado,'Error al listar las traducciones Aprobadas ' mensaje;               
	  END;


	CREATE TEMPORARY TABLE tmpTraducidos 
		(
			tmpid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY
		)
	SELECT 200 Estado, 'OK' Mensaje, CO.idComplemento,CO.Complemento,L.Localizacion LocalizacionOriginal,L1.Localizacion LocalizacionTraduccion, C.Cadena msgid, C1.Cadena msgstr,ifnull(count(BC.idDetalleComplemento),0) numeroAprobaciones
	from Cadena C
	inner join Cadena C1 on C.idDetalleComplemento = C1.idCadenaOriginal
	inner join Localizacion L on C.idLocalizacion = L.idLocalizacion
	inner join Localizacion L1 on C1.idLocalizacion = L1.idLocalizacion
	inner join Complemento CO on CO.idComplemento = C.idComplemento
	left join BitacoraCambio BC on BC.idDetalleComplemento = C1.idDetalleComplemento and BC.IdEstado in (select idEstado from estado where estado = 'Aprobado')
    where C1.idDetalleComplemento not in  (select idDetalleComplemento from BitacoraCambio where IdEstado = 7)
	group by CO.idComplemento,CO.Complemento,L.Localizacion,L1.Localizacion, C.Cadena , C1.Cadena;
    
    insert into BitacoraCambio (idDetalleComplemento,idEstado,FEchaCambio,nombreusr,correousr)
	select distinct BC.idDetalleComplemento,7,now(),'Admin','admin@gmail.com'
    from tmpTraducidos T
    inner join Cadena C on C.idComplemento = T.idComplemento and T.msgstr = C.Cadena
    inner join BitacoraCambio BC on BC.idDetalleComplemento = C.idDetalleComplemento and BC.IdEstado = 6
    where numeroAprobaciones = 2;

	select * from tmpTraducidos  where numeroAprobaciones = 2;

	drop table tmpTraducidos;
END