var chai = require('chai')
var chaihttp = require('chai-http')
const expect = require('chai').expect

chai.use(chaihttp)

describe('Testing', function () { // eslint-disable-line
  it('El servicio retorna el id del cliente', function (done) { // eslint-disable-line 
    chai.request('http://localhost:3000')
      .get('/servicio')
      .end(function (err, res) { // eslint-disable-line 
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })

  it('El servicio retorna el piloto enviando el id del cliente', function (done) { // eslint-disable-line 
    chai.request('http://localhost:3000')
      .get('/piloto/3')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })

  it('El servicio retorna la ubicación enviando el id del cliente y el id del piloto', function (done) { // eslint-disable-line
    chai.request('http://localhost:3000')
      .get('/ubicacion/3/8')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
})
